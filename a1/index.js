let num = 2
const getCube = num ** 3
// console.log(getCube);

console.log(`The cube of ${num} is ${getCube}`);

const address = ["258 Washington Drive NW", "California", 90011];
const [street, state, zipCode] = address;
console.log(`I live at ${street}, ${state} ${zipCode}`);

const animal = {
    name: "Lolong",
    animalKind: "Saltwater Crocodile",
    weight: "1075 kgs",
    measurement: "20 ft 3 in"
}

console.log(`${animal.name} is a ${animal.animalKind}. He weight at ${animal.weight} with a measurement of ${animal.measurement}.`)

const numGroup = [1,2,3,4,5];
numGroup.forEach((numGroupItem) => {
    console.log(numGroupItem);
})


const reduceNumber = numGroup.reduce((acc, cur) => acc + cur);
console.log(reduceNumber);


class Dog {
    constructor(name, age, breed){
        this.name = name;
        this.age = age;
        this.breed = breed;

    }
}

const dog1 = new Dog("Frankie", 5, "Miniature DachShund");
console.log(dog1);